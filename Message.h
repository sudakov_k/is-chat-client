#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <cstdint>

//!
//! \brief Message parser class.
//!
//! Message format:
//! [ name_size ] [ text_size ] [ name ] [ text ]
//! - name_size: 2 bytes, size name section;
//! - text_size: 2 bytes, size text section;
//! - name: client name, not null-terminated;
//! - text: client text, not null-terminated.
//!
class CMessage
{
public:
    //!
    //! \brief Decode message.
    //! \param _data Raw data.
    //! \return Decoded message.
    //!
    static CMessage FromRawData(const std::string &_data);

    //!
    //! \brief Construct new message.
    //! \param _name Client name.
    //! \param _text Client text.
    //!
    CMessage(const std::string &_name, const std::string &_text);

    //!
    //! \brief Encode message.
    //! \return Encoded data.
    //!
    std::string ToRawData() const;

    //!
    //! \brief Get client name.
    //! \return Client name.
    //!
    std::string GetName() const;

    //!
    //! \brief Get client text.
    //! \return Client text.
    //!
    std::string GetText() const;

    //!
    //! \brief Get message status.
    //! \return Decoding success.
    //!
    bool GetStatus() const;

    //!
    //! \brief Name section maximum size.
    //!
    static constexpr size_t nameMax = UINT16_MAX;

    //!
    //! \brief Text section maximum size.
    //!
    static constexpr size_t textMax = UINT16_MAX;

private:    
    // Convert uint16_t value to raw data
    static std::string FromU16(uint16_t _value);

    // Convert raw data to uint16_t value
    static uint16_t ToU16(char _high, char _low);

    // Construct invalid message
    CMessage();

    std::string m_Name;     // Client name
    std::string m_Text;     // Client text
    bool m_Status;          // Decoding success
};

#endif // MESSAGE_H
