# is-chat-client #
## Overview ##
is-chat-client - chat client based on [WebSocket++](https://github.com/zaphoyd/websocketpp) library.

## Usage ##
```
#!text

./is-chat-client [args]
args:
-h, --help : help;
-a ADDR, --addr ADDR : network interface address, default: "127.0.0.1";
-p PORT, --port PORT : network port, default: "8080";
-l LOG, --log LOG : log file name, default: "client.log";
-n NAME, --name NAME : client name, default: "anonymous".
```

## Build ##
Dependency:

* WebSocket++ (libwebsocketpp-dev), headers only;
* Asio C++ Library (libasio-dev), headers only;
* POSIX Threads (libpthread-stubs0-dev);
* C++11 support.

Build using makefile and 3rdparty libs:
```
#!sh

cd is-chat-client
make
```

Build using makefile and system libs:
```
#!sh

cd is-chat-client
make USE_SYSTEM_LIBS=1
```

Build using qmake (system libs):
```
#!sh

mkdir build
cd build
qmake ../is-chat-client
make

```
