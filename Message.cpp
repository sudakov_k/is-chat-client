#include "Message.h"

// Aliases
using std::string;

CMessage CMessage::FromRawData(const string &_data)
{
    // Minimum data size is 4 bytes (empty name and text sections)
    if (_data.size() >= 4)
    {
        // Name size: bytes 0, 1
        size_t name_size = ToU16(_data[0], _data[1]);

        // Text size: bytes 2, 3
        size_t text_size = ToU16(_data[2], _data[3]);

        // Check message size
        if (_data.size() == (name_size + text_size + 4))
        {
            // Correct message
            return CMessage(_data.substr(4, name_size), _data.substr(4 + name_size, text_size));
        }
    }

    // Incorrect message
    return CMessage();
}


CMessage::CMessage(const string &_name, const string &_text) :
    m_Name(_name),
    m_Text(_text),
    m_Status(true)
{
}

string CMessage::ToRawData() const
{
    string data;    // Data string

    if (m_Status)
    {
        // Reserve data memory
        data.reserve(4 + m_Name.size() + m_Text.size());

        data += FromU16(m_Name.size()); // Name size section
        data += FromU16(m_Text.size()); // Text size section
        data += m_Name;                 // Name section
        data += m_Text;                 // Text section
    }

    return data;
}

string CMessage::GetName() const
{
    return m_Name;
}

string CMessage::GetText() const
{
    return m_Text;
}

bool CMessage::GetStatus() const
{
    return m_Status;
}

string CMessage::FromU16(uint16_t _value)
{
    string data;    // Data string

    // Reserve 2 bytes
    data.reserve(2);

    // High part, byte 0
    data += static_cast<char>((_value >> 8) & UINT16_C(0x00FF));

    // Low part, byte 1
    data += static_cast<char>(_value & UINT16_C(0x00FF));

    return data;
}

uint16_t CMessage::ToU16(char _high, char _low)
{
    // char -> unsigned char is needed for negative values

    // Low part
    uint16_t value = static_cast<unsigned char>(_low);

    // High part
    value |= (static_cast<uint16_t>(static_cast<unsigned char>(_high)) << 8) & UINT16_C(0xFF00);

    return value;
}

CMessage::CMessage() :
    m_Name(),
    m_Text(),
    m_Status(false)
{
}
