#ifndef LOG_H
#define LOG_H

#include <string>
#include <fstream>
#include <mutex>

//!
//! \brief Log class.
//!
class CLog
{
public:
    //!
    //! \brief Construct object without file.
    //!
    CLog();

    //!
    //! \brief Close file, free resources.
    //!
    ~CLog();

    //!
    //! \brief Open log file.
    //! \param _file_name File name.
    //! \return success.
    //!
    //! MT-safe
    //!
    bool Open(const std::string &_file_name);

    //!
    //! \brief Close log file.
    //!
    //! MT-safe
    //!
    void Close();

    //!
    //! \brief Insert string line to log.
    //! \param _str message.
    //!
    //! MT-safe
    //!
    void Insert(const std::string &_str);

private:
    typedef std::mutex LockT;

    // Return date and time [%Y-%m-%d %H:%M:%S], UTC
    static std::string DateTimeNow();

    LockT m_Lock;           // file mutex
    std::ofstream m_Stream; // file stream
};

//!
//! \brief Log object global variable.
//!
extern CLog g_Log;

#endif // LOG_H
