TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += ASIO_STANDALONE

LIBS += -lpthread

SOURCES += main.cpp \
    Client.cpp \
    Log.cpp \
    Message.cpp \
    CmdArgs.cpp

HEADERS += \
    Client.h \
    Log.h \
    Message.h \
    CmdArgs.h
