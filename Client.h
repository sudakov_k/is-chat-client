#ifndef CLIENT_H
#define CLIENT_H

#include <cstdint>
#include <string>
#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>

//!
//! \brief Network client class.
//!
class CClient
{
public:
    CClient();
    virtual ~CClient();

    //!
    //! \brief Run client.
    //! \param _name Client name.
    //! \param _addr Server network address.
    //! \param _port Server network port.
    //! \return Exit code.
    //!
    int Run(const std::string &_name, const std::string &_addr, uint16_t _port);

    //!
    //! \brief Shutdown client.
    //!
    void Shutdown();

private:
    typedef websocketpp::lib::asio::io_service ServiceT;
    typedef websocketpp::lib::asio::signal_set SignalSetT;
    typedef websocketpp::lib::asio::streambuf InputBufferT;
    typedef websocketpp::lib::asio::posix::stream_descriptor StreamT;
    typedef websocketpp::lib::error_code ErrorCodeT;
    typedef websocketpp::client<websocketpp::config::asio_client> ClientT;
    typedef websocketpp::connection_hdl ConnectionHandlerT;

    // Signals (SIGINT, SIGTERM) handler
    void SignalHandler(const ErrorCodeT &_error_code, int _signal_number);

    // Open connection handler
    void OpenHandler(ConnectionHandlerT _connection_hdl);

    // Connection close handler
    void CloseHandler(ConnectionHandlerT _connection_hdl);

    // Connection fail handler
    void FailHandler(ConnectionHandlerT _connection_hdl);

    // Message handler
    void MessageHandler(ConnectionHandlerT _connection_hdl, ClientT::message_ptr _message);

    // Console input handler
    void InputHandler(const ErrorCodeT &_error_code, size_t _size);

    // Get server information (URI)
    std::string GetServerInfo(ConnectionHandlerT _connection_hdl);

    ServiceT m_Service;                 // Async service
    SignalSetT m_SignalSet;             // Signals receiver
    ClientT m_Client;                   // Server
    StreamT m_Input;                    // Console input descriptor
    InputBufferT m_InputBuf;            // Console input buffer
    ConnectionHandlerT m_Connection;    // Connection handler
    std::string m_Name;                 // Client name
};

#endif // CLIENT_H
