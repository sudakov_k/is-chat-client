#include <iostream>
#include "Client.h"
#include "Message.h"
#include "Log.h"

// Aliases
namespace placeholders = websocketpp::lib::placeholders;
namespace opcode = websocketpp::frame::opcode;
using std::string;
using std::cout;
using std::endl;
using websocketpp::lib::bind;
using websocketpp::lib::make_shared;
using websocketpp::lib::asio::buffer;
using websocketpp::lib::asio::buffer_cast;
using websocketpp::lib::asio::async_read_until;
using websocketpp::exception;
using websocketpp::uri;
using websocketpp::uri_ptr;
using websocketpp::log::alevel;

CClient::CClient() :
    m_Service(),
    m_SignalSet(m_Service),
    m_Client(),
    m_Input(m_Service, dup(STDIN_FILENO)),
    m_InputBuf(CMessage::textMax),
    m_Connection(),
    m_Name("anonymous")
{
    m_Client.init_asio(&m_Service);
}

CClient::~CClient()
{
    // Close console input
    m_Input.close();
}

int CClient::Run(const string &_name, const string &_addr, uint16_t _port)
{
    // Set client name
    m_Name = _name;

    // exit code: 0 - ok, other - fail
    int exit_code = -1;

    g_Log.Insert("run");

    try
    {
        // Disable console logger
        m_Client.clear_access_channels(alevel::all);
        m_Client.clear_error_channels(alevel::all);

        // Configure signal receiver
        m_SignalSet.add(SIGINT);
        m_SignalSet.add(SIGTERM);
        m_SignalSet.async_wait(bind(&CClient::SignalHandler, this, placeholders::_1, placeholders::_2));

        // Configure client
        m_Client.set_open_handler(bind(&CClient::OpenHandler, this, placeholders::_1));
        m_Client.set_close_handler(bind(&CClient::CloseHandler, this, placeholders::_1));
        m_Client.set_fail_handler(bind(&CClient::FailHandler, this, placeholders::_1));
        m_Client.set_message_handler(bind(&CClient::MessageHandler, this, placeholders::_1, placeholders::_2));

        // Configure console input
        // Read while ENTER not clicked
        async_read_until(m_Input, m_InputBuf, "\n",
                         bind(&CClient::InputHandler, this, placeholders::_1, placeholders::_2));

        // Convert _addr and _port to URI
        uri_ptr location = make_shared<uri>(false, _addr, _port, "/");

        // Prepare connection
        ErrorCodeT ec;
        ClientT::connection_ptr con = m_Client.get_connection(location, ec);

        // Fail prepare connection
        if (ec)
        {
            throw exception(ec);
        }

        // Print connection parameters
        cout << "connect: " << location->str() << endl;

        // Log connect
        g_Log.Insert("connect: " + location->str());

        // Connect client to server
        m_Client.connect(con);

        // Start async service
        m_Service.run();

        // Server stopped, exit code 0
        exit_code = 0;
    }
    catch (const exception &e)
    {
        g_Log.Insert(e.what());
    }
    catch (...)
    {
        g_Log.Insert("unknown exception");
    }

    return exit_code;
}

void CClient::Shutdown()
{
    // Print "shutdown"
    cout << "shutdown" << endl;

    // Log shutdown
    g_Log.Insert("shutdown");

    // Stop service, cances all async operations
    if (!m_Service.stopped())
    {
        m_Service.stop();
    }
}

void CClient::SignalHandler(const ErrorCodeT &_error_code, int /* _signal_number */)
{
    // Shutdown
    if (!_error_code)
    {
        Shutdown();
    }
}

void CClient::OpenHandler(ConnectionHandlerT _connection_hdl)
{
    // Save connection handler
    m_Connection = _connection_hdl;

    // Print "connected"
    cout << "connected" << endl;

    // Log connecting
    g_Log.Insert("connected: " + GetServerInfo(_connection_hdl));
}

void CClient::CloseHandler(CClient::ConnectionHandlerT _connection_hdl)
{
    // Remove connection handler
    m_Connection.reset();

    // Print "disconnected"
    cout << "disconnected" << endl;

    // Log disconnecting
    g_Log.Insert("disconnected: " + GetServerInfo(_connection_hdl));

    // Shutdown client
    Shutdown();
}

void CClient::FailHandler(ConnectionHandlerT _connection_hdl)
{
    // Remove connection handler
    m_Connection.reset();

    // Print "connection fail"
    cout << "connection fail" << endl;

    // Log connection fail
    g_Log.Insert("connection fail: " + GetServerInfo(_connection_hdl));

    // Shutdown client
    Shutdown();
}

void CClient::MessageHandler(ConnectionHandlerT _connection_hdl, ClientT::message_ptr _message)
{
    if (_message)
    {
        // Check incoming message type
        if (_message->get_opcode() == opcode::binary)
        {
            CMessage msg = CMessage::FromRawData(_message->get_payload());

            if (msg.GetStatus())
            {
                // Correct message
                // Print message
                cout << "[" << msg.GetName() << "] " << msg.GetText() << endl;
            }
            else
            {
                // Log incorrect message
                g_Log.Insert("received incorrect message: " + GetServerInfo(_connection_hdl));
            }

        }
        else
        {
            // Log incorrect message
            g_Log.Insert("received incorrect message: " + GetServerInfo(_connection_hdl));
        }
    }
}

void CClient::InputHandler(const ErrorCodeT &_error_code, size_t _size)
{
    if (!_error_code && (_size > 0) && (_size <= CMessage::textMax))
    {
        // Send error code
        ErrorCodeT send_ec;

        // Get text data
        string text(buffer_cast<const char *>(m_InputBuf.data()), _size);

        // Clear buffer
        m_InputBuf.consume(_size);

        // Remove '/n'
        text.pop_back();

        // Send test message
        m_Client.send(m_Connection, CMessage(m_Name, text).ToRawData(), opcode::binary, send_ec);

        if (send_ec)
        {
            cout << "error send: " << send_ec.message() << endl;
        }

        // Read while ENTER not clicked
        async_read_until(m_Input, m_InputBuf, "\n",
                         bind(&CClient::InputHandler, this, placeholders::_1, placeholders::_2));
    }
}

string CClient::GetServerInfo(ConnectionHandlerT _connection_hdl)
{
    string str;     // Server URI string

    try
    {
        // Get connection from handler
        ClientT::connection_ptr con = m_Client.get_con_from_hdl(_connection_hdl);

        // Get URI
        str = con->get_uri()->str();
    }
    catch (const exception & /* e */)
    {
        str = "unknown";
    }

    return str;
}
